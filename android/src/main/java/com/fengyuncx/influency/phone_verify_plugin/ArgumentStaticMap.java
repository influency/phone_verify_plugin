package com.fengyuncx.influency.phone_verify_plugin;

import android.util.SparseArray;
import android.util.SparseIntArray;

import java.util.HashMap;

public interface ArgumentStaticMap {

    public static final String METHOD_INIT_SDK= "initSdk";
    public static final String METHOD_SET_PRIVACY= "setPrivacy";
    public static final String METHOD_CHECK_ENV= "checkEnv";
    public static final String METHOD_LOGIN= "login";
    public static final String INVOKE_METHOD_QUIT_LOGIN= "quitLogin";
    public static final String PARAM_AUTH_KEY = "authKey";
    public static final String PARAM_PRIVACY_ISSUE_NAME = "privacyIssueName";
    public static final String PARAM_PRIVACY_ISSUE_URL = "privacyIssueUrl";
    public static final String PARAM_USER_ISSUE_NAME = "userIssueName";
    public static final String PARAM_USER_ISSUE_URL = "userIssueUrl";

    public static final String INVOKE_METHOD_ISP="serverProvider";
    public static final String INVOKE_METHOD_LOGIN_TOKEN="loginToken";
    public static final String INVOKE_METHOD_ON_CLICK="onClick";

}
