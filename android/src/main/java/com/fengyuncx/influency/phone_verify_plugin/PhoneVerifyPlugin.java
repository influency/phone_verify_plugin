package com.fengyuncx.influency.phone_verify_plugin;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;

/** PhoneVerifyPlugin */
public class PhoneVerifyPlugin implements FlutterPlugin ,ActivityAware{
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity

  PhoneNumVerifyHandler mVerifyHandler;
  public static void registerWith(PluginRegistry.Registrar registrar) {
    PhoneNumVerifyHandler verifyHandler = new PhoneNumVerifyHandler(registrar.context().getApplicationContext(),registrar.activity());
    registrar.addViewDestroyListener(verifyHandler);
    verifyHandler.startListening(registrar.messenger());
  }



  public PhoneVerifyPlugin(){
    mVerifyHandler = new PhoneNumVerifyHandler();
  }

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    mVerifyHandler.setApplicationContext(flutterPluginBinding.getApplicationContext());
    mVerifyHandler.setActivity(null);
    mVerifyHandler.startListening(flutterPluginBinding.getBinaryMessenger());
  }
  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    mVerifyHandler.stopListening();
    mVerifyHandler.setApplicationContext(null);
    mVerifyHandler.setActivity(null);


  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    mVerifyHandler.setActivity(binding.getActivity());
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    onDetachedFromActivity();
  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
    onAttachedToActivity(binding);
  }

  @Override
  public void onDetachedFromActivity() {
    mVerifyHandler.setActivity(null);
  }
}
