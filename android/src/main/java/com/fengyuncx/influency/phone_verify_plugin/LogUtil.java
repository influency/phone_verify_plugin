package com.fengyuncx.influency.phone_verify_plugin;

import android.util.Log;

public class LogUtil {
    private static boolean isDebug = BuildConfig.DEBUG;

    public static void logV(String TAG,String text){
        if(isDebug){
            Log.v(TAG,text);
        }
    }
    public static void logE(String TAG,String text){
        if(isDebug){
            Log.e(TAG,text);
        }
    }
    public static void logD(String TAG,String text){
        if(isDebug){
            Log.d(TAG,text);
        }
    }
    public static void logI(String TAG,String text){
        if(isDebug){
            Log.i(TAG,text);
        }
    }
    public static void logW(String TAG,String text){
        if(isDebug){
            Log.w(TAG,text);
        }
    }
}
