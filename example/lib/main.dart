import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:phone_verify_plugin/phone_verify_plugin.dart';



void main() {
  runApp(MyApp());
  PhoneVerifyPlugin.init(
    authKeyAndroid:
    'upqXep6FsOOjIHaZ7mAYGgHqa76I9Y5EOuYOqMOt9tijMAUUbvgvOJE5rJnIfGHsIfq3Q0X4LlllgVUBYs8WFmBytOipe69/Wx+j0ziSoMe8mVNs6DeR9R7Q291/nFyMicjIxoNkxxWOXEfgcrIYwarh4puFQnEwpHWwqQS0TvXjph6lDCxu7CMFSxtLZQAblwh8wclAWqgEaki0tvA7r+MoT3pUENv0IDam50z75OhS595AuN56buscDXcSeHK8M6M9WZxwjN4h8jYzbcVSbEL8BTPTE16Po4gbgGgSrhPyJpPFupJJTaJ072Vof+q3',
    authKeyIOS: 'HCBgAdvsKO/6w3HHXCXCyNbY3ZVdhV4hlhh8xyT3ASgePWDjx5I6wrUW1GcMiae5aZsPmvHTo/Dc83nQ7EwfR4CLLuNaa0iteF0W2FC4oC7QRgu3I6HMCqGvAw43jSnq93UyrKs1noHJcvbs851kXPH5Acef4LbuBgvyMyfUXHjLvKwkbQ0vs9q1laXH/FyfRCVKCXPvlZXhEXOzjQSPdLZ6MrrFwlTPF7whpj+G247nHp+lFRMKJ8xDMQVjUwNeNj5Zts45JGRu+N3ACpeg/w==',
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  Timer delayedTask;

  @override
  void initState() {
    super.initState();
    initPlatformState();
    PhoneVerifyPlugin.instance.verifyResp().listen((data) {
      if (data == null) {
        print('_MyAppState.initState data null');
        return;
      }
      String code = data.code;
      if (code == '500000') {
        print('_MyAppState.initState: getISP:${data?.ISP}');
      } else if (code == '600000') {
        print('_MyAppState.initState: token = ${data.token}');
        //mock the network request
        delayedTask = Timer(Duration(seconds: 1), () {
          PhoneVerifyPlugin.instance.quitLogin();
        });
      } else if (code == '600011') {
        print('_MyAppState.initState: token error = ${data.message}');
      }
    });
    PhoneVerifyPlugin.instance.onClickResp().listen((event) {
      if (event == null) {
        return;
      }
      String code = event.code;
      print('_MyAppState.initState code = $code');
      print('_MyAppState.initState json = ${event.json}');
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion = 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    PhoneVerifyPlugin.instance.checkEnv();
    PhoneVerifyPlugin.instance.setPrivacy(
      privacyIssueName: '《风韵出行隐私协议》',
      userIssueName: '《风韵出行用户协议》',
      privacyIssueUrl: 'http://www.baidu.com',
      userIssueUrl: 'http://www.baidu.com',);
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: RaisedButton(
            child: Text('login'),
            onPressed: () => PhoneVerifyPlugin.instance.quickLogin(),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    if (delayedTask != null) {
      delayedTask.cancel();
    }
    super.dispose();
  }
}
