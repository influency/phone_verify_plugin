//
//  PNSBuildModelUtils.m
//  phone_verify_plugin
//
//  Created by influency on 2021/5/11.
//

#import "PNSBuildModelUtils.h"
#define TX_SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define TX_SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define IS_HORIZONTAL (TX_SCREEN_WIDTH > TX_SCREEN_WIDTH)

#define TX_Alert_NAV_BAR_HEIGHT      55.0
#define TX_Alert_HORIZONTAL_NAV_BAR_HEIGHT      41.0

//竖屏弹窗
#define TX_Alert_Default_Left_Padding         42
#define TX_Alert_Default_Top_Padding          115

/**横屏弹窗*/
#define TX_Alert_Horizontal_Default_Left_Padding      80.0

@implementation PNSBuildModelUtils

+ (TXCustomModel *)buildFullScreenModel: (NSString*)privacyName privacyUrl:(NSString*)privacyUrl userIssueName:(NSString*)userIssueName userIssueUrl:(NSString*)userIssueUrl {
    TXCustomModel* model = [[TXCustomModel alloc] init];
    
    
    model.navColor = UIColor.whiteColor;
    model.navTitle = [[NSAttributedString alloc] initWithString:@"风韵出行" attributes:@{NSForegroundColorAttributeName : UIColor.blackColor,NSFontAttributeName : [UIFont systemFontOfSize:20.0]}];
    model.navIsHidden = NO;
    model.navBackImage = [UIImage imageNamed:@"icon_close_gray"];
    
    //model.hideNavBackItem = NO;
//    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    // [rightBtn setTitle:@"更多" forState:UIControlStateNormal];
//    model.navMoreView = rightBtn;
    
    
    model.privacyNavColor = UIColor.whiteColor;
    model.privacyNavBackImage = [UIImage imageNamed:@"icon_nav_back_gray"];
    model.privacyNavTitleFont = [UIFont systemFontOfSize:20.0];
    model.privacyNavTitleColor = UIColor.blackColor;
    
    model.logoImage = [UIImage imageNamed:@"icon_logo.png"];

    model.numberColor = [self colorWithHex:0x282B31 alpha: 1];
    model.numberFont = [UIFont systemFontOfSize: 30.0];

    model.loginBtnText = [[NSAttributedString alloc] initWithString:@"一键登录" attributes:@{NSForegroundColorAttributeName : UIColor.whiteColor,NSFontAttributeName : [UIFont systemFontOfSize:16.0]}];
    model.loginBtnBgImgs = @[
      [UIImage imageNamed:@"btn_orange"],
      [UIImage imageNamed:@"btn_orange"],
      [UIImage imageNamed:@"btn_orange"]
    ];
    model.loginBtnHeight = 48;
    model.loginBtnLRPadding = 40;
//    UIImage* loginBtnImage = [self imageWithRoundedCornersSize: [self imageWithColor:UIColor.orangeColor] cornerRadius:5.0f];
//    model.loginBtnBgImgs = @[
//        loginBtnImage,
//        loginBtnImage,
//    ];
    model.autoHideLoginLoading = NO;


    model.privacyOne = @[userIssueName,userIssueUrl];
    model.privacyTwo = @[privacyName,privacyUrl];
    
    model.privacyColors = UIColor.blackColor;
    model.privacyAlignment = NSTextAlignmentCenter;
    model.privacyFont = [UIFont fontWithName:@"PingFangSC-Regular" size:14.0];
//    model.privacyPreText = @"我已阅读同意";
    model.privacyOperatorPreText = @"《";
    model.privacyOperatorSufText = @"》";
    // 是否同意
    model.checkBoxIsChecked = YES;
    model.checkBoxIsHidden = YES;
    model.checkBoxWH = 18.0;
//    model.checkBoxImages = @[
//      [UIImage imageNamed:@"icon_uncheck"],
//      [UIImage imageNamed:@"icon_checked"],
//    ];

     model.changeBtnTitle = [[NSAttributedString alloc] init];
    model.changeBtnTitle = [
       [NSAttributedString alloc] initWithString:@"其他方式登录"
       attributes:@{NSForegroundColorAttributeName: [self colorWithHex:0x676C75 alpha: 1], NSFontAttributeName : [UIFont systemFontOfSize:14.0]}
    ];
    model.changeBtnIsHidden = NO;

    model.prefersStatusBarHidden = NO;
    model.preferredStatusBarStyle = UIStatusBarStyleLightContent;
    //model.presentDirection = PNSPresentationDirectionBottom;
    
    //授权页默认控件布局调整
    //model.navBackButtonFrameBlock =
    //model.navTitleFrameBlock =
//    model.navMoreViewFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
//        CGFloat width = superViewSize.height;
//        CGFloat height = width;
//        return CGRectMake(superViewSize.width - 15 - width, 0, width, height);
//    };
//    model.loginBtnFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
//        if ([self isHorizontal:screenSize]) {
//            frame.origin.y = 20;
//            return frame;
//        }
//        return frame;
//    };
//    model.sloganFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
//        if ([self isHorizontal:screenSize]) {
//            return CGRectZero; //横屏时模拟隐藏该控件
//        } else {
//            return CGRectMake(0, 140, superViewSize.width, frame.size.height);
//        }
//    };
//    model.numberFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
//        if ([self isHorizontal:screenSize]) {
//            frame.origin.y = 140;
//        }
//        return frame;
//    };
//    model.loginBtnFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
//        if ([self isHorizontal:screenSize]) {
//            frame.origin.y = 185;
//        }
//        return frame;
//    };
//    model.changeBtnFrameBlock = ^CGRect(CGSize screenSize, CGSize superViewSize, CGRect frame) {
//        if ([self isHorizontal:screenSize]) {
//            return CGRectZero; //横屏时模拟隐藏该控件
//        } else {
//            return CGRectMake(10, frame.origin.y, superViewSize.width - 20, 30);
//        }
//    };
//
    return model;
}

+ (UIColor *)colorWithHex:(NSInteger)hex alpha:(CGFloat)alpha {
    return [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0 green:((float)((hex & 0xFF00) >> 8))/255.0 blue:((float)(hex & 0xFF))/255.0 alpha:alpha];
}

+(UIImage*) imageWithColor:(UIColor*)color{
    CGRect rect = CGRectMake(0.0f, 0.0f, 3.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

//生成圆角UIIamge 的方法
+ (UIImage *)imageWithRoundedCornersSize:(UIImage*)image cornerRadius: (float)cornerRadius
{
    UIImage *original = image;
    CGRect frame = CGRectMake(0, 0, original.size.width, original.size.height);
    // 开始一个Image的上下文
    UIGraphicsBeginImageContextWithOptions(original.size, NO, 1.0);
    // 添加圆角
    [[UIBezierPath bezierPathWithRoundedRect:frame
                                cornerRadius:cornerRadius] addClip];
    // 绘制图片
    [original drawInRect:frame];
    // 接受绘制成功的图片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/// 是否是横屏 YES:横屏 NO:竖屏
+ (BOOL)isHorizontal:(CGSize)size {
    return size.width > size.height;
}


@end

