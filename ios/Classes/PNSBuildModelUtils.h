//
//  PNSBuildModelUtils.h
//  phone_verify_plugin
//
//  Created by influency on 2021/5/11.
//

#ifndef PNSBuildModelUtils_h
#define PNSBuildModelUtils_h


#endif /* PNSBuildModelUtils_h */

#import <Foundation/Foundation.h>
#import <ATAuthSDK/ATAuthSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface PNSBuildModelUtils : NSObject

/// 创建全屏的model
+ (TXCustomModel *)buildFullScreenModel:(NSString*)privacyName
    privacyUrl:(NSString*)privacyUrl
    userIssueName:(NSString*)userIssueName
    userIssueUrl:(NSString*)userIssueUrl;

@end

NS_ASSUME_NONNULL_END
