import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:phone_verify_plugin/phone_verify_onclick_resp_model.dart';
import 'package:phone_verify_plugin/phone_verify_resp_model.dart';

class PhoneVerifyPlugin {
  final MethodChannel _channel =
      const MethodChannel('gitee.com/influency/phone_verify_plugin',JSONMethodCodec());
  static const String _INVOKE_METHOD_INIT_SDK = "initSdk";
  static const String _INVOKE_METHOD_SET_PRIVACY = "setPrivacy";
  static const String _INVOKE_METHOD_CHECK_ENV = "checkEnv";
  static const String _INVOKE_METHOD_LOGIN = "login";
  // static const String _INVOKE_METHOD_VERIFY_PHONE = "verifyPhone";
  static const String _INVOKE_METHOD_QUIT_LOGIN = "quitLogin";
  static const String _PARAM_AUTH_KEY = "authKey";
  static const String _PARAM_PRIVACY_ISSUE_NAME = "privacyIssueName";
  static const String _PARAM_PRIVACY_ISSUE_URL = "privacyIssueUrl";
  static const String _PARAM_USER_ISSUE_NAME = "userIssueName";
  static const String _PARAM_USER_ISSUE_URL = "userIssueUrl";

  ///--- method call from platform:
  static const String _METHOD_CALL_LOGIN_TOKEN = "loginToken";
  static const String _METHOD_CALL_ISP = "serverProvider";
  static const String _METHOD_CALL_ON_CLICK= "onClick";


  final StreamController<PhoneVerifyResp> _verifyRespController = StreamController<PhoneVerifyResp>.broadcast();
  final StreamController<PhoneVerifyOnClickResp> _onClickRespController = StreamController<PhoneVerifyOnClickResp>.broadcast();

  static PhoneVerifyPlugin _instance;
  factory PhoneVerifyPlugin()=>_instance;
  static PhoneVerifyPlugin get instance => _instance;
  static void init({
      @required String authKeyAndroid,
      @required String authKeyIOS}) {
    _instance = PhoneVerifyPlugin._();
    _instance._registerApp(
      authKeyAndroid: authKeyAndroid,
      authKeyIOS: authKeyIOS,
    );
  }
  PhoneVerifyPlugin._() {
    _channel.setMethodCallHandler(_handleCall);
  }

  Future<void> _registerApp({
    @required String authKeyAndroid,
    @required String authKeyIOS,
  }) {
    String authKey;
    if(Platform.isAndroid){
      authKey = authKeyAndroid;
    }else if(Platform.isIOS){
      authKey = authKeyIOS;
    }else{
      throw Exception("platform method not implement");
    }
    assert(authKey?.isNotEmpty ?? false);
    return _channel
        .invokeMethod<void>(_INVOKE_METHOD_INIT_SDK, <String, dynamic>{
      _PARAM_AUTH_KEY: authKey,
    });
  }


  Future<void> setPrivacy(
      {String privacyIssueName,
    String privacyIssueUrl,
    String userIssueName,
    String userIssueUrl,}) async{
    return await _channel.invokeMethod<void>(_INVOKE_METHOD_SET_PRIVACY, <String, dynamic>{
      _PARAM_PRIVACY_ISSUE_NAME: privacyIssueName,
      _PARAM_PRIVACY_ISSUE_URL: privacyIssueUrl,
      _PARAM_USER_ISSUE_NAME: userIssueName,
      _PARAM_USER_ISSUE_URL: userIssueUrl,
    });
  }
  Future<void> checkEnv() async{
    return await _channel.invokeMethod<void>(_INVOKE_METHOD_CHECK_ENV, null);
  }
  Future<void> quickLogin() async{
    return await _channel.invokeMethod<void>(_INVOKE_METHOD_LOGIN, null);
  }

  Future<void> quitLogin() async{
    return await _channel.invokeMethod<void>(_INVOKE_METHOD_QUIT_LOGIN, null);
  }

  Future<void> _handleCall(MethodCall call) {
    Map arguments = call.arguments as Map;
    switch (call.method) {
      case _METHOD_CALL_ISP:
        print('PhoneVerifyPlugin._handleCall ISP = ${json.encode(arguments)}');
        _verifyRespController.add(PhoneVerifyResp.fromJson(arguments));
        break;
      case _METHOD_CALL_LOGIN_TOKEN:
        _verifyRespController.add(PhoneVerifyResp.fromJson(arguments));
        break;
      case _METHOD_CALL_ON_CLICK:
        _onClickRespController.add(PhoneVerifyOnClickResp.fromJson(arguments));
        break;
    }
  }

  Stream<PhoneVerifyResp> verifyResp(){
    return _verifyRespController.stream;
  }
  Stream<PhoneVerifyOnClickResp> onClickResp(){
    return _onClickRespController.stream;
  }
}
