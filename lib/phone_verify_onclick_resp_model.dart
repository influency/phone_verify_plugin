
class PhoneVerifyOnClickResp{
  String code;
  String json;

  PhoneVerifyOnClickResp({this.code,this.json});

  factory PhoneVerifyOnClickResp.fromJson(Map json){
    return PhoneVerifyOnClickResp(
      code: json['code']??"",
      json: json['json']??"",
    );
  }
  Map<String,dynamic> toJson(){
    return {
      "code":code,
      "json":json,
    };
  }
}