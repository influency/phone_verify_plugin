
class PhoneVerifyResp{
  String code;
  String message;
  String token;
  String ISP;

  PhoneVerifyResp({this.code,this.ISP, this.message, this.token});

  factory PhoneVerifyResp.fromJson(Map json){
    return PhoneVerifyResp(
      code: json['code']??"",
      message: json['message']??"",
      token: json['token']??"",
      ISP: json['ISP']??"",
    );
  }
  Map<String,dynamic> toJson(){
    return {
      "code":code,
      "message":message,
      "token":token,
      "ISP":ISP,
    };
  }
}